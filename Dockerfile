FROM python:3.9.18

COPY ./requirements.txt ./
COPY main.py ./

RUN pip install -r requirements.txt

CMD ["python3","main.py"]
