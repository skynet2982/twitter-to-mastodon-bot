import os
import datetime
import time
import redis
from tweeterpy import TweeterPy
from tweeterpy.util import Tweet
from mastodon import Mastodon

#########################
# Environment variables #
#########################
# Global
fetch_time_minutes = int(os.getenv('FETCH_TIME_MINUTES', 60))
hashtags_to_add = os.getenv('HASHTAGS_TO_ADD')
# Redis
redis_host = os.getenv('REDIS_HOST', 'localhost')
redis_port = int(os.getenv('REDIS_PORT', "6379"))
# Twitter
twitter_account_to_retrieve = os.getenv('TWITTER_ACCOUNT_TO_RETRIEVE')
tweets_to_retrieve = int(os.getenv('TWEETS_TO_RETRIEVE', 5))
twitter_log = os.getenv('TWITTER_LOGIN')
twitter_pwd = os.getenv('TWITTER_PASSWORD')
# Mastodon
mastodon_login = os.getenv('MASTODON_LOGIN')
mastodon_password = os.getenv('MASTODON_PASSWORD')
mastodon_instance = os.getenv('MASTODON_INSTANCE')

#############
# Constants #
#############
twitter_session_path = 'Twitter Saved Sessions/session.pkl'
mastodon_clientcred_secret = 'clientcred.secret'
mastodon_usercred_secret = 'usercred.secret'

# Twitter Session
twitter = TweeterPy()

if os.path.exists(twitter_session_path):
    print("Twitter session already exist.")
else:
    print("Generating twitter session...")
    twitter_session = twitter.login(twitter_log, twitter_pwd)
    twitter.save_session(session=twitter_session, session_name='session')
    print("Session saved !")

twitter.load_session(twitter_session_path)

# Connect to Redis
redis_client = redis.Redis(host=redis_host, port=redis_port, decode_responses=True)

# Configure Mastodon
Mastodon.create_app(
    'twitter-to-mastodon-bot',
    api_base_url=mastodon_instance,
    to_file=mastodon_clientcred_secret
)

mastodon_client = Mastodon(client_id=mastodon_clientcred_secret, )
mastodon_client.log_in(
    mastodon_login,
    mastodon_password,
    to_file=mastodon_usercred_secret
)

mastodon_client = Mastodon(access_token=mastodon_usercred_secret)


def push_last_tweets_to_mastodon():
    # Get last tweets
    user_tweets = twitter.get_user_tweets(twitter_account_to_retrieve,
                                          total=tweets_to_retrieve + 1)  # +1 because return not only tweets
    tweets_data = user_tweets['data']

    for key in tweets_data:
        # Check entryId to be sure we are processing a tweet
        entry_id = str(key['entryId'])

        if entry_id.startswith("tweet"):
            # clean the data
            tweet_obj = Tweet(key)

            # Check if present in Redis
            tweet_obj_id = redis_client.get(tweet_obj.id_str)

            if tweet_obj_id is None:
                if tweet_obj.retweeted:
                    tweet = tweet_obj.full_text[len(tweet_obj.full_text) - 1]
                else:
                    tweet = tweet_obj.full_text

                if hashtags_to_add is not None:
                    tweet = tweet + "\n\n"

                    hashtags = hashtags_to_add.split(",")
                    for hashtag in hashtags:
                        tweet = tweet + "#" + hashtag
                        tweet = tweet + " "

                mastodon_client.toot(tweet)
                redis_client.set(tweet_obj.id_str, "ValueNotUsed")


delta_hour = 0

while True:
    now_hour = datetime.datetime.now().hour
    if delta_hour != now_hour:
        # run your code
        push_last_tweets_to_mastodon()

        delta_hour = now_hour

        time.sleep(60 * fetch_time_minutes)

        # add some way to exit the infinite loop
