[🇬🇧 English version](media/README.md)

# Presentation

**twitter-to-mastodon-bot** is a tool for sending tweets from a Twitter account 
to a Mastodon account.

# Self-hosting

A Docker image is available here: https://hub.docker.com/repository/docker/nogafam/twitter-to-mastodon-bot

## Configuration

You need a Twitter account and a Mastodon account.
You'll also need a Redis instance.

Set the following environment variables:

| Key | Description                            | Required | Default |
|---------------------------------|----------------------------------------|-------------|-------------------|
| **FETCH_TIME_MINUTES** | Time between tweets fetch (in minutes) | No | 60 |
**HASHTAGS_TO_ADD** | List of hashtags to add to each tweet  | No ||
**REDIS_HOST** | Redis host                             | No |redis|
| **REDIS_PORT** | Port Redis                             | No |6379|
|TWITTER_ACCOUNT_TO_RETRIEVE** | Twitter account to clone               | Yes ||
|TWEETS_TO_RETRIEVE** | Number of tweets to retrieve each time | No |5|
| **MASTODON_INSTANCE** | Url of Mastodon instance               | Yes ||
| **MASTODON_LOGIN** | Login Mastodon                         | Yes ||
| Mastodon_PASSWORD** | Mastodon password                      | Yes || 
| **TWITTER_LOGIN** | Login Twitter                          | Yes ||
|TWITTER_PASSWORD** | Twitter Password                       | Yes ||

An example of Docker Compose is available here: [docker-compose.yml.example](docker-compose.yml.example)

# License

**twitter-to-mastodon-bot** is distributed under the [AGPLV3](https://www.gnu.org/licenses/agpl-3.0.fr.html) license.

![license](https://www.gnu.org/graphics/agplv3-155x51.png)
