[🇬🇧 English Version](media/README.md)

# Présentation

**twitter-to-mastodon-bot** est un outil qui permet d'envoyer les tweets d'un compte 
Twitter vers un compte Mastodon.

# Auto Hébergement

Une image Docker est disponible ici : https://hub.docker.com/repository/docker/nogafam/twitter-to-mastodon-bot

## Configuration

Vous avez besoin d'un compte Twitter et d'un compte Mastodon.
Il vous faut aussi une instance de Redis.

Les variables d'environnement suivantes sont à configurer:

| Clé                             |Description| Obligatoire | Valeur par défaut |
|---------------------------------|-----------------------------------------------------|-------------|-------------------|
| **FETCH_TIME_MINUTES**          |Temps entre deux recupération des tweets (en minutes)| Non         | 60                |
| **HASHTAGS_TO_ADD**             |Liste des hashtags à ajouter à chaque tweets| Non         ||
| **REDIS_HOST**                  |Hôte Redis| Non         |redis|
| **REDIS_PORT**                  |Port Redis| Non         |6379|
| **TWITTER_ACCOUNT_TO_RETRIEVE** |Compte tweet à clone| Oui         ||
| **TWEETS_TO_RETRIEVE**          |Nombre de tweets à récupérer à chaque fois| Non         |5|
| **MASTODON_INSTANCE**           |Url de l'instance Mastodon| Oui         ||
| **MASTODON_LOGIN**              |Login Mastodon| Oui         ||
| **MASTODON_PASSWORD**           |Mot de passe Mastodon| Oui         || 
| **TWITTER_LOGIN**               |Login Twitter| Oui         ||
|**TWITTER_PASSWORD**|Mot de passe Twitter| Oui         ||

Un exemple de Docker Compose est dispo ici : [docker-compose.yml.example](docker-compose.yml.example)

# Licence

**twitter-to-mastodon-bot** est distribué sous licence [AGPLV3](https://www.gnu.org/licenses/agpl-3.0.fr.html)

![licence](https://www.gnu.org/graphics/agplv3-155x51.png)
